package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Query;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.data.database.CityDao;
import fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.OWMInterface;
import fr.uavignon.ceri.tp3.data.webservice.WeatherResponse;
import fr.uavignon.ceri.tp3.data.webservice.WeatherResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase.databaseWriteExecutor;

public class WeatherRepository {

    private static final String TAG = WeatherRepository.class.getSimpleName();

    private LiveData<List<City>> allCities;
    private MutableLiveData<City> selectedCity;

    MutableLiveData<Boolean> isLoading;
    MutableLiveData<Throwable> webServiceThrowable;

    private CityDao cityDao;

    private final OWMInterface api;

    private static volatile WeatherRepository INSTANCE;

    private volatile int nbAPIloads;


    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }


    public MutableLiveData<Throwable> getWebServiceThrowable() {
        return webServiceThrowable;
    }

    public synchronized static WeatherRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new WeatherRepository(application);

        }

        return INSTANCE;
    }

    public WeatherRepository(Application application) {
        WeatherRoomDatabase db = WeatherRoomDatabase.getDatabase(application);
        cityDao = db.cityDao();
        allCities = cityDao.getAllCities();
        selectedCity = new MutableLiveData<>();

        isLoading = new MutableLiveData<Boolean>();
        webServiceThrowable = new MutableLiveData<Throwable>();

        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://api.openweathermap.org/data/2.5/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();
        api = retrofit.create(OWMInterface.class);
    }

    public LiveData<List<City>> getAllCities() {
        return allCities;
    }

    public MutableLiveData<City> getSelectedCity() {
        return selectedCity;
    }




    public long insertCity(City newCity) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return cityDao.insert(newCity);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(newCity);
        return res;
    }

    public int updateCity(City city, Boolean updateSelectedCity) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return cityDao.update(city);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (updateSelectedCity && res != -1)
            selectedCity.setValue(city);
        return res;
    }


    // Surcharge de la methode pour simplifier l'appel de updateCity dans le cas standard ou updateSelectedCity est à true
    public int updateCity(City city) {
        return updateCity(city, true);
    }


    public void deleteCity(long id) {
        databaseWriteExecutor.execute(() -> {
            cityDao.deleteCity(id);
        });
    }

    public void getCity(long id)  {
        Future<City> fcity = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return cityDao.getCityById(id);
        });
        try {
            selectedCity.setValue(fcity.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public List<City> getSynchrAllCities(){
        Future<List<City>> cityList = databaseWriteExecutor.submit(() -> {
           Log.d(TAG, "Retriving all cities");
           return cityDao.getSynchrAllCities();
        });
        try {
            return cityList.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void updateLoadingState(MutableLiveData<Boolean> loadingState){
        loadingState.postValue(isLoading.getValue());
    }

    public void loadWeatherCity(City city) {
        isLoading.setValue(true);
        String key = "89d8d7cb474e45716dfcee16a27f20ca";
        Log.d("Request : ", city.getName() + ",," +city.getCountryCode().toString());
        api.getWeather(city.getName() + ",," +city.getCountryCode().toString(), key).enqueue(
                new Callback<WeatherResponse>() {
                    @Override
                    public void onResponse(Call<WeatherResponse> call,
                                           Response<WeatherResponse> response) {
                        Log.d("Call :", call.toString());
                        WeatherResult.transferInfo(response.body(), city);

                        updateCity(city, selectedCity.getValue() == null || city.getId() == selectedCity.getValue().getId());
                        // met à jour selectedCity seulement lorsqu'il s'agit de la ville affichée dans detailFragment ou quand aucune ville n'est selectionnée

                        nbAPIloads -= 1;
                        if (nbAPIloads <= 0) isLoading.postValue(false);  // < 0 : in the case nbAPIload was 0 (when called in DetailFragment)
                    }

                    @Override
                    public void onFailure(Call<WeatherResponse> call, Throwable t) {
                        Log.d("Call :", call.toString());
                        Log.d("FailureLWC : ", t.getMessage());
                        System.out.println("----------------------STACK TRACE-----------------------");
                        StringWriter sw = new StringWriter();
                        PrintWriter pw = new PrintWriter(sw);
                        t.printStackTrace(pw);
                        String sStackTrace = sw.toString(); // stack trace as a string
                        System.out.println(sStackTrace);
                        System.out.println("--------------------------------------------------------");

                        webServiceThrowable.postValue(t);
                        nbAPIloads -= 1;
                        if (nbAPIloads <= 0) isLoading.postValue(false);  // < 0 : in the case nbAPIload was 0 (when called in DetailFragment)
                    }
                });

    }



    public void loadWeatherAllCities(){
        List<City> synchrAllCities = getSynchrAllCities();
        nbAPIloads = synchrAllCities.size();

        for (City city : synchrAllCities) {
            loadWeatherCity(city);
        }
    }


}
