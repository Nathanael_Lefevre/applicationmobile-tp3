package fr.uavignon.ceri.tp3.data.webservice;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface OWMInterface {
    /*
    @Headers("Accept: application/geo+json")
    @GET("/gridpoints/{office}/{gridX},{gridY}/forecast")
    Call<WeatherResponse> getForecast(@Path("office") String office,
                                      @Path("gridX") int gridX,
                                      @Path("gridY") int gridY);

 */

    @Headers({
            "Accept: json",
    })
    @GET("weather")
    public Call<WeatherResponse> getWeather(@Query("q") String q, @Query("APIkey") String apiKey);
}
