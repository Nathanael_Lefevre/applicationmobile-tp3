package fr.uavignon.ceri.tp3.data.webservice;

import java.io.Serializable;
import java.util.List;

public class WeatherResponse {
    public final Main main = null;
    public final Wind wind = null;
    public final Clouds clouds = null;
    public final List<Weather> weather = null;

    public final Integer dt = null;


    static class Main {
        public final Float temp = null;
        public final Integer humidity = null;
    }

    static class Wind {
        public final Float speed;
        public final Integer deg;

        Wind() {
            this.speed =  null;//Float.parseFloat(null);
            this.deg = null; //Integer.parseInt(null);
        }
    }

    static class Clouds{
        public final Integer all;

        Clouds() {
            this.all = null; //Integer.parseInt(null);
        }
    }

    static class Weather {
        public final String description;
        public final String icon;

        Weather(){
            this.description = null;
            this.icon = null;
        }
    }
}
