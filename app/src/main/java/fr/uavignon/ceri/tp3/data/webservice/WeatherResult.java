package fr.uavignon.ceri.tp3.data.webservice;

import java.util.Date;
import java.util.List;

import fr.uavignon.ceri.tp3.data.City;
import fr.uavignon.ceri.tp3.data.WeatherRepository;

public class WeatherResult {

    /*
    public final boolean isLoading;
    public final Forecast forecast;
    public final Throwable error;

    public WeatherResult(boolean isLoading, Forecast forecast, Throwable error) {
        this.isLoading = isLoading;
        this.forecast = forecast;
        this.error = error;
    }

     */

    public static void transferInfo(WeatherResponse weatherInfo, City cityInfo){
        cityInfo.setTempKelvin(weatherInfo.main.temp);
        cityInfo.setHumidity(weatherInfo.main.humidity);
        cityInfo.setWindSpeedMPerS((int)(float) weatherInfo.wind.speed);
        cityInfo.setWindDirection(weatherInfo.wind.deg);
        cityInfo.setCloudiness(weatherInfo.clouds.all);

        cityInfo.setLastUpdate(weatherInfo.dt);
        cityInfo.setIcon(weatherInfo.weather.get(0).icon);
        cityInfo.setDescription(weatherInfo.weather.get(0).description);

        //cityInfo.setLastUpdate(new Date().getTime());
    }
}
